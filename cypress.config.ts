import { defineConfig } from "cypress";

export default defineConfig({

  defaultCommandTimeout: 30000,
  projectId: "ahgb68",
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://www.vangoghmuseum.nl/'
  },
});
