# Gerimedica Assignment - Cypress
## _by chanaka u._

## Installation

Cypress requires [Node.js](https://nodejs.org/) v10+ to run.

Install the dependencies and devDependencies below.

```sh
1. git clone https://gitlab.com/uchanake/gerimedica.git
2. cd ./gerimedica
3. npm install cypess --save-dev
4. npm install typescript --save-dev
5. npm i
`````````

## Run tests

```
npm run test