import { CookiesPanel } from '../panels/CookiesPanel';
import {CollectionPage} from '../pages/CollectionPage';

export class HomePage {

    private cookiesPanel: CookiesPanel;

    private linkCollection: string = 'a[href="/nl/collectie"]';

    constructor() {
        this.cookiesPanel = new CookiesPanel();
    }

    NavigateToHomePage(): HomePage {
        cy.VisitTo('/');
        this.cookiesPanel.AcceptCookies();
        return this;
    }

    NavigateToCollectionPage() {
        cy.ClickElement(this.linkCollection);
        return new CollectionPage();
    }
}