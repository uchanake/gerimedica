import { CookiesPanel } from '../panels/CookiesPanel';
import { ImagePage } from './ImagePage';

export class CollectionPage {

    private cookiesPanel: CookiesPanel;

    private btnSearch: string = '.search-field-input-wrapper ~.search-field-search-button';
    private lstImages: string = '.collection-art-object-list-item';
    private txtSearch: string = '.search-field-input';
    private iconLoader: string = '.loader';
    private txtSearchCount: string = '.results';
    private btnClose: string = '.icon-close-big';

    constructor() {
        this.cookiesPanel = new CookiesPanel();
    }

    NavigateToCollectionPage(): CollectionPage {
        cy.VisitTo(Cypress.config().baseUrl + Cypress.env('locale').lan_du + Cypress.env('page_uri').collection);
        this.cookiesPanel.AcceptCookies();
        cy.SaveValueOfSelector(this.txtSearchCount, 'temp_data', 'defaulrResultsCount');
        return this;
    }

    SearchPaint(searchTerm: string) {
        cy.Type(this.txtSearch, searchTerm);
        cy.ClickElement(this.btnSearch);
        cy.CheckElemeVisible(this.iconLoader);
        cy.WaitForElementDismiss(this.iconLoader);
        return this;
    }

    ValidateCollectionPageLoaded(): CollectionPage {
        cy.ValidateUrl(Cypress.config().baseUrl + Cypress.env('locale').lan_du + Cypress.env('page_uri').collection);
        cy.CheckElemeVisible(this.txtSearchCount);
        cy.CheckElemeVisible(this.btnSearch);
        cy.CheckElemeVisible(this.lstImages)
        return this;
    }

    ValidateTheResultCount(resultThresholdValue:number) {
        cy.AssertSearchResultCount(this.txtSearchCount, resultThresholdValue);
        return this;
    }

    SelectTheImage(nth_Image: number) {
        cy.SelectFromElementList(this.lstImages, nth_Image);
        return new ImagePage();
    }

    ResetSearch() {
        cy.ClickElement(this.btnClose);
        cy.WaitForElementDismiss(this.iconLoader);
        return this;
    }
}