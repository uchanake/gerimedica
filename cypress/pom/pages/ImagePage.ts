import { CookiesPanel } from "pom/panels/CookiesPanel";

export class ImagePage {

    private cookiesPanel: CookiesPanel;

    private lstBtnInformation: string = '.accordion-item-button button';
    private lstTxtInformation: string = '.definition-list-item-value';

    constructor() {
        this.cookiesPanel = new CookiesPanel();
    }

    ExpandImageInfo(infoType): ImagePage {
        switch (infoType) {
            case 'ObjData':
                cy.SelectFromElementList(this.lstBtnInformation, 1);
                break;
            case 'Exhi':
                cy.SelectFromElementList(this.lstBtnInformation, 2);
                break;
            case 'Lit':
                cy.SelectFromElementList(this.lstBtnInformation, 3);
                break;
        }
        return this;
    }

    ValidateImageProperties(imageInfo): ImagePage {
        cy.AssertElementTextOfArray(this.lstTxtInformation, 0, imageInfo.f_number);
        cy.AssertElementTextOfArray(this.lstTxtInformation, 1, imageInfo.jh_number);
        cy.AssertElementTextOfArray(this.lstTxtInformation, 2, imageInfo.inventory_number);
        return this;
    }
}