export class CookiesPanel {

    private btnAcceptCookies: string = '.cookie-banner-button.btn';

    constructor() { }

    AcceptCookies(): void {
        cy.ClickElement(this.btnAcceptCookies);
    }
}