/// <reference types = 'cypress' />

import { HomePage } from '../pom/pages/HomePage';

const homePage = new HomePage();

describe('validate home page redirections', () => {

    beforeEach(() => {
        homePage
            .NavigateToHomePage()
    })

    it('validate search page navigation', () => {
        homePage
            .NavigateToCollectionPage()
            .ValidateCollectionPageLoaded()
    });
});