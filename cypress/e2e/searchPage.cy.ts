/// <reference types = 'cypress' />

import { CollectionPage } from "pom/pages/CollectionPage";
import { images } from '../testData/ImageData.json';

const collectionPage = new CollectionPage();

beforeEach(() => {
    collectionPage
        .NavigateToCollectionPage()
        .SearchPaint("Het Gele Huis");
});

describe('validate search page functions', () => {

    it('validate results comes on the search', () => {
        collectionPage
            .ValidateTheResultCount(700)
            .ResetSearch()
    });

    it('validate search result 1st image as expected', () => {
        collectionPage
            .SelectTheImage(1)
            .ExpandImageInfo(images[0].infoTypes.objectData)
            .ValidateImageProperties(images[0].object_data)
    });
});