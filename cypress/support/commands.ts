/// <reference types="cypress" />

import { StringNullableChain } from "cypress/types/lodash"

Cypress.Commands.add('VisitTo', (url: string) => {
    cy.visit(url);
});

Cypress.Commands.add('ClickElement', (selector: string) => {
    cy.get(selector).should('be.visible').click();
});

Cypress.Commands.add('ValidateUrl', (url: string) => {
    cy.url().should('eq', url)
})

Cypress.Commands.add('CheckElemeVisible', (selector: string) => {
    cy.get(selector).should('be.visible');
});

Cypress.Commands.add('Type', (selector: string, text: string) => {
    cy.get(selector).should('be.visible').type(text);
});

Cypress.Commands.add('WaitForElementDismiss', (selector: string) => {
    cy.get(selector).should('not.exist');
});

Cypress.Commands.add('SaveValueOfSelector', (selector: string, fileName: string, variableName: string) => {
    cy.get(selector).invoke('text').then(el => {
        const data = {};
        data[variableName] = el.trim();
        cy.UpdateFile(fileName, data);
    });
});

Cypress.Commands.add('UpdateFile', (fileName: string, jsonData: object) => {
    cy.readFile(`cypress/fixtures/${fileName}.json`).then(data => {
        const merged = Object.assign(data, jsonData);
        cy.writeFile(`cypress/fixtures/${fileName}.json`, merged);
    });
});

Cypress.Commands.add('ReturnTextOfElement', (selector: string) => {
    cy.get(selector).should('be.visible');
    return cy.get(selector).invoke('text').then((text) => {
        return text;
    });
});

Cypress.Commands.add('AssertSearchResultCount', (selector: string, count: number) => {
    cy.fixture('temp_data').then((data) => {
        cy.ReturnTextOfElement(selector).should((returnCount) => {
            expect(Number(returnCount)).to.be.at.most(Number(data.defaulrResultsCount));
            expect(Number(returnCount)).greaterThan(count);
        });
    });
});

Cypress.Commands.add('SelectFromElementList', (selector: string, index: number) => {
    if (index > 0) {
        cy.get(selector).its('length').should('be.gt', index);
        cy.get(selector).should('be.visible').eq(index);
        cy.get(selector).eq(index - 1).click();
    }
});

Cypress.Commands.add('AssertElementTextOfArray', (selector: string,index:number, value: string) => {
    cy.get(selector).each(($ele, i) => {
        if (index === i) {
            expect($ele.text()).equal(value);
        }
    });
});

declare global {
    namespace Cypress {
        interface Chainable {
            /**
             * Navigate to the base URL
             * @param baseUrl 
             * @example cy.VisitTo('/')
             */
            VisitTo(baseUrl: string): Chainable<void>

            /**
             * Click element
             * @param selector
             * @example cy.ClickElement('#boo')
             */
            ClickElement(selector: string): Chainable<void>

            /**
             * Validate the URL with current page URL
             * @param URL
             * @example cy.ValidateUrl('www.google.lk')
             */
            ValidateUrl(url: string): Chainable<void>

            /**
             * Checke elemennt visible
             * @param locator
             * @param text
             * @example cy.CheckElemeVisible('.boo', 'hello')
             */
            CheckElemeVisible(selector: string): Chainable<void>

            /**
             * Type on element
             * @param selector
             * @param text
             * @example cy.Type('#boo', 'hello')
             */
            Type(selector: string, text: string): Chainable<void>

            /**
             * Wait for dismiss the element
             * @param selector
             * @example cy.WaitForElementDismiss('[id="spinner"]')
             */
            WaitForElementDismiss(selector: string): Chainable<void>

            /**
             * Save value in a given file under given key
             * @param selector
             * @param filePath
             * @param keyValue
             * @example cy.saveTextOfSelector('[id="spinner"]', './data.json', 'name')
             */
            SaveValueOfSelector(selector: string, fileName: string, keyValue: string): Chainable<void>

            /**
             * Update file with json data
             * @param fileName 
             * @param jsonData 
             * @example cy.UpdateFile('./data.json', '{"name":"boo"}')
             */
            UpdateFile(fileName: string, jsonData: object): Chainable<void>

            /**
             * Return text of element
             * @param selector
             * @example cy.ReturnTextOfElement('#boo')
             */
            ReturnTextOfElement(selector: string): Chainable<string>

            /**
             * Return fixture file value of the key
             * @param selecot 
             * @param count 
             * @example cy.AssertSearchResultCount('#boo', 100)
             */
            AssertSearchResultCount(selector: string, count: number): Chainable<void>

            /**
             * Select given nth:element from array of elements
             * @param selector 
             * @param nth_index
             * @example cy.SelectFromElementList('#boo', 2)
             */
            SelectFromElementList(selector: string, nth_index: number): Chainable<void>

            /**
             * Check element text
             * @param selector 
             * @param property 
             * @example cy.CheckElementText('#boo', 'hello')
             */
            CheckElementText(selector: string, property: string): Chainable<void>

            /**
             * Assert the elemennt text of array
             * @param selector
             * @param inndex
             * @param expectedValue
             * @example cy.AssertElementTextOfArray('#boo', 1, 'hello!')
             */
            AssertElementTextOfArray(selector: string, index: number, expectedValue:string):Chainable<void>
        }
    }
}